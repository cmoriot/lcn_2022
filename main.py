import requests
import pandas as pd
import ipaddress
from pprint import pprint
import csv
from rdap_verif import *
import sys

#process d'analyse des données contenues dans Wikidata
#step one : extract data from WIKIDATA having IPv4 routing prefix
def query_ip_in_wiki():
    url = 'https://query.wikidata.org/sparql'

    query="""SELECT ?plage_IPv4 ?item ?itemLabel ?itemAltLabel ?rank WHERE {
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
  OPTIONAL {
    ?item p:P3761 ?statement.
    ?statement ps:P3761 ?plage_IPv4;
      wikibase:rank ?rank.
  }
}"""
    headers = {'User-Agent':"IPSeen/0.0"}

    r = requests.get(url,params = {'format': 'json', 'query': query},headers=headers)
    if r.status_code == 200:
        return r.json()
    if r.status_code == 500:
        print("wiki : 500 request code")
        return None
    if r.status_code == 403:
        print("wiki : 403 request code")
        return None
    if r.status_code == 429:
        timeout = get_delay(r.headers['retry-after'])
        print('Timeout {} m {} s'.format(timeout // 60, timeout % 60))
        time.sleep(timeout)
        query_ip_in_wiki()

#step 2 clean les infos : retirer là ou il y a que les RIR, et les addresses IP privées
def clean_file(df_wiki_block):
    df_special_range = pd.read_csv('iana-ipv4-special-registry-1.csv')
    for index_wiki, row_wiki in df_wiki_block.iterrows():
        for index_iana, row_iana in df_special_range.iterrows():
            try:
                if (ipaddress.IPv4Network(row_wiki['plage_ipv4'],False).overlaps(ipaddress.IPv4Network(row_iana['Address Block'],False))):
                    df_wiki_block.drop(index_wiki, axis=0, inplace=True)
                elif ipaddress.IPv4Network(row_wiki['plage_ipv4']).is_multicast:
                    df_wiki_block.drop(index_wiki, axis=0, inplace=True)
            except Exception as e:
                print(e)
                print(f"{row_wiki['plage_ipv4']} at {index_wiki} in wiki file")
                print(f"{row_iana['Address Block']} at {index_iana} in iana file")
    RIR_list=['Asia-Pacific Network Information Centre','African Network Information Center','American Registry for Internet Numbers','Latin America and Caribbean Network Information Centre','Réseaux IP Européens Network Coordination Centre','AFNIC']
    for index_wiki, row_wiki in df_wiki_block.iterrows():
        for RIR_name in RIR_list:
            if RIR_name == row_wiki['name']:
                df_wiki_block.drop(index_wiki, axis=0, inplace=True)
            if isinstance(row_wiki['itemAltLabel'],str) and RIR_name in row_wiki['itemAltLabel']:
                df_wiki_block.drop(index_wiki, axis=0, inplace=True)
    return df_wiki_block

def create_file_ip_in_wiki():
    res =query_ip_in_wiki()
    res=res["results"]["bindings"]
    rows = []
    for data in res:
        row={}
        row['item']=data['item']['value']
        row['name']=data['itemLabel']['value']
        row['itemAltLabel']=data.get('itemAltLabel',{}).get('value')
        row['plage_ipv4']=data['plage_IPv4']['value']
        row['rank']=data['rank']['value']
        rows.append(row)
    df = pd.DataFrame(rows)
    df = clean_file(df)
    df.to_csv('ip_in_wiki.csv', index = False, header=True)

def recursive_check(val,data):
    res = False
    mot = None
    if isinstance(data, dict):
        for k,v in data.items():
            res,mot = recursive_check(val,v)
            if res == True:
                return True, mot
        return False,None
    # Si le paramètre est une liste
    elif isinstance(data,list):
        for elem in data:
            res,mot = recursive_check(val,elem)
            if res == True:
                return True,mot
        return False,None

    # Si le paramètre est une chaine
    elif isinstance(data,str):
        data = data.lower()
        if val in data:
            mot = val
            return True,mot
        else :
            return False,None
    return None,None

#step 3 récuperer les infos RDAP
def check_in_RDAP(item,alt_item,rdap_data):
    item = item.lower()
    alt_item = alt_item.lower()
    item, itemmot = recursive_check(item, rdap_data)
    itemalt, itemaltmot = False, None
    if alt_item != "":
        itemalt, itemaltmot = recursive_check(alt_item,rdap_data)
    if item and itemalt:
        return item,itemmot
    elif item and not itemalt:
        return item, itemmot
    elif not item and itemalt :
        return itemalt , itemaltmot
    else:
        return False,None

def RDAP_query(ip):
    req = IPWhois(ip)
    result = req.lookup_rdap(inc_nir=True,asn_methods=['dns', 'whois', 'http'],rate_limit_timeout=1,retry_count=3)
    time.sleep(0.5)
    return result


def main():
    #creation d'un file propre avec les infos de wikidata
    create_file_ip_in_wiki()
    #ouverture du fichier et récuprération des données sous la forme d'un dictionnaire clé/valeur, clé = ip, valeur = dict du tableau
    wiki_range_list=[]
    wiki_file_annuaire={}
    wiki_file_annuaire_alt={}
    with open('ip_in_wiki.csv') as wikicsv:
        read = csv.reader(wikicsv, delimiter=',',quotechar='"')
        next(read)
        for row in read:
            if row[3] in wiki_range_list :
                print(row[3])
            wiki_range_list.append(row[3])
            wiki_file_annuaire[row[3]]=row[1]
            wiki_file_annuaire_alt[row[3]]=row[2]
    #print(len(wiki_range_list))
    wiki_range_list = list(dict.fromkeys(wiki_range_list))
    #print(len(wiki_range_list))
    not_found_list = []

    compteur_ok =0
    compteur_missing = 0
    compteur_part_name = 0
    compteur_manual = 0
    compteur_no_result =0
    #step 3 récuperer les infos RDAP
    for net in wiki_range_list:
        ip=net.split('/')[0]
        data=RDAP_query(ip)
        res, mot = check_in_RDAP(wiki_file_annuaire[net],wiki_file_annuaire_alt[net],data)
        Found = False
        if res :
            compteur_ok +=1
            Found = True
            #print(wiki_file_annuaire[net],wiki_file_annuaire_alt[net],mot)
        else:
            item_list = wiki_file_annuaire[net].split()
            alt_item_list = []
            if wiki_file_annuaire_alt[net] != "":
                alt_item_list = wiki_file_annuaire_alt[net].split()
            total_list = [*item_list,*alt_item_list]
            txt_file = open("text_exception_list.txt", "r")
            exception_words = txt_file.read()
            for word in total_list :
                res, mot = check_in_RDAP(word,'',data)
                if res and len(mot)>3 and mot not in exception_words:
                    compteur_part_name +=1
                    print(word,mot)
                    Found = True
                    break
    #get result from manually classified file
    file_name = sys.argv[1]
    df_manual_classification = pd.read_csv(file_name)
    for ip in not_found_list:
        res = df_manual_classification.loc[df_manual_classification['query'] == ip,['result']].iat[0,0]
        if res == 1:
            compteur_manual +=1
        else :
            compteur_no_result +=1
    total_found = compteur_ok + compteur_part_name + compteur_manual
    if (total_found + compteur_missing + compteur_no_result) == len(wiki_range_list):
        print(f"{compteur_ok} found values, {compteur_missing} missing RDAP info, {compteur_part_name} part name found, {compteur_manual} found with manual analysis, {compteur_no_result} not found, total = {len(wiki_range_list)}")

        #results graphics
        fig, axes = plt.subplots(1,1,figsize=(15, 6), subplot_kw=dict(aspect="equal"))
        labels=['COHERENT','NOT COHERENT']
        colors = ['#44B5CE', '#97D077']
        sizes = numpy.array([total_found,compteur_no_result])
        wedges, texts, autotexts = axes.pie(sizes, autopct=lambda pct: func(pct, sizes),colors = colors,pctdistance=1.3,labeldistance = 1)
        axes.set_title('Wikidata IPv4 analysis', fontsize =20)
        plt.setp(autotexts, size=20)
        axes.legend(wedges, labels,
                  title="Legend",
                  loc="center left",
                  bbox_to_anchor=(1.2, 0, 0, 1),
                  prop={'size': 20})

        plt.setp(autotexts, size=20)
        plt.show()

    else:
        print("error in process")

if __name__ == '__main__':
    main()
